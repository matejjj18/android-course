package hr.fer.ruazosa.prvadomacazadaca;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import util.Complex;

public class GrafActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        Complex prviOperand = (Complex)intent.getSerializableExtra("prviOperand");
        Complex drugiOperand = (Complex) intent.getSerializableExtra("drugiOperand");
        Complex rezultat = (Complex) intent.getSerializableExtra("rezultat");

        XYSeries prviOperandseries = new XYSeries("Prvi kompleksni broj");
        prviOperandseries.add(0,0);
        prviOperandseries.add(prviOperand.getReal(), prviOperand.getImaginary());

        XYSeries drugiOperandseries = new XYSeries("Drugi kompleksni broj");
        drugiOperandseries.add(0,0);
        drugiOperandseries.add(drugiOperand.getReal(), drugiOperand.getImaginary());

        XYSeries rezultatseries = new XYSeries("Rezultat broj");
        rezultatseries.add(0,0);
        rezultatseries.add(rezultat.getReal(), rezultat.getImaginary());



        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        dataset.addSeries(prviOperandseries);
        dataset.addSeries(drugiOperandseries);
        dataset.addSeries(rezultatseries);

        XYSeriesRenderer prviOperandRenderer = new XYSeriesRenderer();
        prviOperandRenderer.setColor(Color.RED);
        prviOperandRenderer.setPointStyle(PointStyle.CIRCLE);
        prviOperandRenderer.setFillPoints(true);
        prviOperandRenderer.setLineWidth(2);
        prviOperandRenderer.setDisplayChartValues(true);

        XYSeriesRenderer drugiOperandRenderer = new XYSeriesRenderer();
        drugiOperandRenderer.setColor(Color.GREEN);
        drugiOperandRenderer.setPointStyle(PointStyle.CIRCLE);
        drugiOperandRenderer.setFillPoints(true);
        drugiOperandRenderer.setLineWidth(2);
        drugiOperandRenderer.setDisplayChartValues(true);


        XYSeriesRenderer rezultatRenderer = new XYSeriesRenderer();
        rezultatRenderer.setColor(Color.BLUE);
        rezultatRenderer.setPointStyle(PointStyle.CIRCLE);
        rezultatRenderer.setFillPoints(true);
        rezultatRenderer.setLineWidth(2);
        rezultatRenderer.setDisplayChartValues(true);

        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setXLabels(6);
        multiRenderer.setYLabels(6);
        multiRenderer.setGridColor(Color.BLACK);
        multiRenderer.setMarginsColor(Color.WHITE);

        multiRenderer.setChartTitle("Grafički prikaz kompleksnih brojeva");
        multiRenderer.setXTitle("Realni dio");
        multiRenderer.setYTitle("Imaginarni dio");
        multiRenderer.setZoomButtonsVisible(true);
        DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
        float val = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 18, metrics);
        multiRenderer.setLabelsTextSize(val);
        multiRenderer.setAxisTitleTextSize(val);
        multiRenderer.setChartTitleTextSize(val+5);
        multiRenderer.setLegendTextSize(val);


        multiRenderer.addSeriesRenderer(prviOperandRenderer);
        multiRenderer.addSeriesRenderer(drugiOperandRenderer);
        multiRenderer.addSeriesRenderer(rezultatRenderer);
        multiRenderer.setShowGrid(true);
        View chartView = ChartFactory.getLineChartView(getBaseContext(), dataset,multiRenderer);

        setContentView(chartView);

    }


}
